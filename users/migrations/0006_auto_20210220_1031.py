# Generated by Django 3.1.5 on 2021-02-20 07:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_user_rand_pass'),
    ]

    operations = [
        migrations.RenameField(
            model_name='company',
            old_name='company_email_address',
            new_name='email_address',
        ),
        migrations.RenameField(
            model_name='company',
            old_name='company_phone_number',
            new_name='phone_number',
        ),
        migrations.AlterField(
            model_name='company',
            name='name',
            field=models.CharField(max_length=250, verbose_name='Company Name'),
        ),
    ]
