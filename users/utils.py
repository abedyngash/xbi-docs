# import cryptocode
from django.conf import settings
from django.contrib.auth import get_user_model

User = get_user_model()

def create_wizard_formset_objects(_list):
	if _list:
		non_empty_list = [i for i in _list if bool(i)]
		for i in non_empty_list:
			if i['DELETE'] == True:
				i['id'].delete()
			else:
				objects_to_pop = [str('DELETE'), 'user']
				final_dict = dict([(k, v) for k,v in i.items() if k not in objects_to_pop])
				if final_dict['id'] == None:
					obj = _model.objects.create(user=_user, **final_dict)

def add_bulk_users(_list, _company):
	if _list:
		non_empty_list = [i for i in _list if bool(i)]
		for i in non_empty_list:
			user = User(
				first_name=i['first_name'],
				last_name=i['last_name'],
				email=i['email'],
				company=_company
			)
			password = User.objects.make_random_password()
			user.set_password(password)
			user.rand_pass = password
			user.save()