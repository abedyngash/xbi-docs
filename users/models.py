from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class Company(models.Model):
	''' GENERAL INFO '''
	name = models.CharField('Company Name', max_length=250)
	phone_number = models.CharField('Phone Number', max_length=20, null=True)
	email_address = models.EmailField('Company Email', unique=True, null=True)

	def __str__(self):
		return f'{self.name}'

class User(AbstractUser):
	username = None
	email = models.EmailField(unique=True)
	phone_number = models.CharField('Phone Number', max_length=20, null=True)
	company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='users', null=True)
	is_company_admin = models.BooleanField(default=False)
	is_first_login = models.BooleanField(default=False)
	rand_pass = models.CharField(max_length=255, null=True)

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = []
