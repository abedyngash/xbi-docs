
from django.shortcuts import redirect
from django.contrib import messages

def user_is_company_admin_required(function):
	"""
	Determines whether the user sending this request is a company admin (See ``users.models.User.is_company_admin`` for more details)
	and than allows/denies the user access to the view at hand. 

	"""
	def wrap(request, *args, **kwargs):
		if request.user.is_company_admin:
			return function(request, *args, **kwargs)
		else:
			messages.error(request, 'You don\'t have enough permissions to access that page')
			return redirect(request.META.get('HTTP_REFERER', 'home'))
	wrap.__doc__ = function.__doc__
	wrap.__name__ = function.__name__
	return wrap

def verify_company_info(function):
	"""
	Verifies whether the user sending this request is associated with a company registered in the system.

	If the particular user is not assoiciated with any company, the function returns a 403 Forbidden error.
	"""
	def wrap(request, *args, **kwargs):
		if request.user.is_anonymous:
			return function(request, *args, **kwargs)
		else:
			if request.user.is_company_admin:
				if request.user.company == None:
					return redirect('company-setup')
			return function(request, *args, **kwargs)
	wrap.__doc__ = function.__doc__
	wrap.__name__ = function.__name__
	return wrap