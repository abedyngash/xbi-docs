from django.shortcuts import redirect
from django import forms
from django.contrib import messages
from django.contrib.auth import login, get_user_model
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

from allauth.account.forms import LoginForm, SignupForm

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column, Field, Div, HTML, ButtonHolder
from crispy_forms.bootstrap import AppendedText, PrependedText

from bootstrap_modal_forms.forms import BSModalModelForm

from .models import Company

User = get_user_model()
''' LOGIN FORM '''
class CustomLoginForm(LoginForm):
	"""docstring for LoginForm"""
	def __init__(self, *args, **kwargs):
		super(CustomLoginForm, self).__init__(*args, **kwargs)

		self.helper = FormHelper()
		self.helper.form_tag = False
		self.fields["login"].label = ""
		self.fields["password"].label = ""

		self.helper.layout = Layout(
			Field('login', placeholder="Email Address"),
			AppendedText('password', '<i class="fas fa-eye-slash" id="eye" onclick="showHidePwd();"></i>', placeholder="Enter Password"),

			Field('remember'),
		)


''' USER REGISTRATION FORM '''
class RegistrationForm(UserCreationForm):
	class Meta:
		model = User
		fields = [
			'first_name', 'last_name', 'email', 'phone_number',
			'password1', 'password2'
		]

		labels = {
			'first_name': 'First Name',
			'last_name': 'Last Name',
		}

	def signup(self, request, user):
		login(request, user)
		sweetify.success(request, title='Signup Successful', toast=True, position='top-end', 
			showConfirmButton=False, persistent='&times;', timer=5000
		)
		return redirect('company-setup')

	def save(self, commit=False):
		user = super(RegistrationForm, self).save(commit=False)
		user.is_company_admin = True
		user.save()
		return user

	def __init__(self, *args, **kwargs):
	    super(RegistrationForm, self).__init__(*args, **kwargs)
	    self.fields['first_name'].required=True
	    self.fields['last_name'].required=True

	    self.fields['password2'].label = 'Confirm Password'
	    self.fields['password1'].help_text=''
	    self.fields['password2'].help_text=''

	    self.helper = FormHelper()
	    self.helper.form_tag = False

	    self.helper.layout = Layout(	    	
			Row(
				Column(
					Field('first_name', css_id="", css_class=""),
    			),
				Column(
					Field('last_name', css_id="", css_class=""),
    			),
	    	),
	    	
	    	Row(
    			Column(
					Field('email', css_id="", css_class=""),
    			),
    			Column(
					Field('phone_number', css_class='phone_input'),
    			),
    		),	
    		Row(
    			Column(
					AppendedText('password1', '<i class="fas fa-eye-slash" id="eye" onclick="showHidePwd();"></i>'),
    			),
    			Column(
					AppendedText('password2', '<i class="fas fa-eye-slash" id="eye2" onclick="showHidePwd();"></i>'),
    			)
    		),
	    )

''' COMPANY FORMSET FORMS '''
class CompanyGeneralInfoForm(forms.ModelForm):
	class Meta:
		model = Company
		fields = ['name', 'phone_number', 'email_address']
	def __init__(self, *args, **kwargs):
	    super(CompanyGeneralInfoForm, self).__init__(*args, **kwargs)
	    self.fields['phone_number'].required=False
	    self.fields['email_address'].required=False
	    self.helper = FormHelper()
	    self.helper.form_tag = False
	    self.helper.layout = Layout(
	    	Row(
	    		Column(Field('name')),
	    	),
	    	Row(
	    		Column(Field('phone_number', css_class='phone_input', required=False)),
	    		Column(Field('email_address', required=False)),
	    	),
	    	ButtonHolder(
		    	HTML(
		    		'{% include "users/forms/wizards/_buttons.html" %}'
		    	)
	    	)
	    )

''' TEAM MEMBERS FORM'''
class TeamMembersForm(forms.ModelForm):
	class Meta:
		model = User
		fields = ['first_name', 'last_name', 'email']

TeamMembersFormSet = forms.formset_factory(TeamMembersForm, extra=1)

''' USER UPDATE FORM '''
class TeamUserUpdateForm(BSModalModelForm):
	class Meta:
		model = User
		fields = ['first_name', 'last_name', 'email']

class UserProfileUpdateForm(forms.ModelForm):
	class Meta:
		model = User
		fields = ['first_name', 'last_name', 'email', 'phone_number']

	def __init__(self, *args, **kwargs):
	    super(UserProfileUpdateForm, self).__init__(*args, **kwargs)
	    self.helper = FormHelper()
	    self.helper.form_tag = False
	    self.helper.layout = Layout(
	    	Row(
	    		Column(Field('first_name')),
	    		Column(Field('last_name')),
	    	),
	    	Row(
	    		Column(Field('email')),
	    		Column(Field('phone_number', css_class='phone_input'))
	    	),
	    )