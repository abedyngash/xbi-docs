"""
This File contains the URL configurations for the ``users`` app.

**URL CONFIGURATIONS** ::

    urlpatterns = [
		path('', home, name='home'),
		# Load the ``home`` view on ``/users/`` request

		path('users/company-setup/', CompanyRegistrationView.as_view(), name='company-setup'),
		# Load the ``CompanyRegistrationView`` on ``/users/company-setup/`` request

		path('users/team/profile/', profile, name='profile'),
		# Load the ``profile`` view on ``/users/team/profile/`` request

		# --- Rest of Configurations follow suit ---
    ]

**END OF URL CONFIGURATIONS**
"""
from django.urls import path
from .views import (
		home, add_team, CompanyRegistrationView, 
		TeamUserUpdateView, TeamUserDeleteView,
		profile
	)

urlpatterns = [
	path('', home, name='home'),
	path('users/company-setup/', CompanyRegistrationView.as_view(), name='company-setup'),

	path('users/team/profile/', profile, name='profile'),

	path('users/team/', add_team, name='team-management'),
	path('users/team/user_<int:pk>/update', TeamUserUpdateView.as_view(), name='update-team-user'),
	path('users/team/user_<int:pk>/delete', TeamUserDeleteView.as_view(), name='delete-team-user'),
]