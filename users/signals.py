# import cryptocode
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template.loader import render_to_string
from django.core.mail import EmailMessage
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.conf import settings

from django.contrib.sites.models import Site

User = get_user_model()

@receiver(post_save, sender=User)
def send_team_user_registration_email(sender, instance, created, **kwargs):
	if created:
		if not instance.is_company_admin:
			''' SEND REGISTRATION EMAIL '''
			current_site = Site.objects.get_current()
			email_uid = urlsafe_base64_encode(force_bytes(instance.pk))
			rand_pass = instance.rand_pass
			first_name = instance.first_name
			email = instance.email
			email_context = {
				'rand_pass': rand_pass,
				'first_name': first_name,
				'user_email': email,
				'current_site': current_site
			}

			mail_subject = '[XBI Analytics] Registration Notification To XBI'
			message = render_to_string('users/allauth/account/email/team_user_registration_message.html',
				email_context
			)

			email = EmailMessage(
				mail_subject,
				message,
				settings.DEFAULT_FROM_EMAIL,
			    to=[instance.email],
			    reply_to=[settings.DEFAULT_REPLY_EMAIL],
			    headers={'Message-ID': email_uid},
			)

			email.content_subtype = 'html'
			email.send()