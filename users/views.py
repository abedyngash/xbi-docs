import os, sweetify, datetime, json, calendar, fiscalyear
from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from django.urls import reverse_lazy
from django.db.models.functions import TruncMonth, Coalesce, ExtractDay
from django.db.models import Avg, Sum, Q, F, Max, Sum, DurationField, ExpressionWrapper
from django.contrib.humanize.templatetags.humanize import intword, intcomma

from bootstrap_modal_forms.generic import BSModalUpdateView, BSModalDeleteView

from formtools.wizard.views import SessionWizardView

from reports.forms import UploadWizardForm
from reports.tasks import process_progress_upload, process_budget_upload
from reports.models import BudgetUpload, ProgressUpload, Order, Requisition, Budget, Supplier
from reports.utils import percentage_format, months_between_dates, money_format, datetime_days_format, next_month

from .forms import TeamMembersFormSet, CompanyGeneralInfoForm, TeamUserUpdateForm, UserProfileUpdateForm
from .decorators import user_is_company_admin_required, verify_company_info
from .utils import add_bulk_users
from .mixins import CompanyExistsMixin
from .models import Company

User = get_user_model()
fiscalyear.START_MONTH = settings.FISCAL_YEAR_START_MONTH

# Create your views here.
@login_required
@verify_company_info
def home(request):
	''' --- SUMMARIES ---'''
	prs = Requisition.objects.filter(company=request.user.company)
	suppliers = Supplier.objects.filter(company=request.user.company)
	''' ORDERS '''
	pos = Order.objects.filter(company=request.user.company)
	delivered_pos = pos.exclude(vendor_delivery_date=None).exclude(release_date=None).annotate(
        delivery_days=ExpressionWrapper(F('vendor_delivery_date') - F('release_date'), output_field=DurationField()))
	delayed_orders = pos.filter(release_date__gt=datetime.date.today())

	''' PERFORMANCE '''
	delivered_pos_count = delivered_pos.count() or 1
	time_scores = [90 / po.delivery_days.days if po.delivery_days > datetime.timedelta(days=90) else 1 for po in delivered_pos]
	pos_with_delivered_quantity = pos.filter(quantity_delivered__gt=0).annotate(grn_diff=F('quantity_delivered') - F('order_quantity'))
	overall_time_score = sum(time_scores) / delivered_pos_count or 1
	overall_quantity_score = sum([po.quantity_delivered / po.order_quantity if po.grn_diff < 1 else 1 for po in
                                  pos_with_delivered_quantity]) / pos_with_delivered_quantity.count() if pos_with_delivered_quantity.count() else 0

	overall_perfomance_score = (overall_time_score + overall_quantity_score) / 2

	''' FINANCE '''
	total_order_value = pos.aggregate(sum_value=Coalesce(Sum('gross_order_value'), 0))["sum_value"]
	sum_of_paid_amounts = pos.aggregate(Sum('paid_amount'))["paid_amount__sum"] or 1
	sum_of_invoiced_amounts = pos.aggregate(Sum('invoiced_amount'))["invoiced_amount__sum"] or 1

	'''--- END OF SUMMARIES---'''

	'''--- GRAPH DATA ---'''
	print(datetime.datetime.today().year)
	latest_year = Budget.objects.all().order_by('fiscal_year').first().fiscal_year if Budget.objects.first() else datetime.datetime.today().year
	financial_year = f'{fiscalyear.FiscalYear(int(latest_year)).start.year}-{fiscalyear.FiscalYear(int(latest_year)).end.year}'

	total_budget = Budget.objects.aggregate(Sum('amount'))['amount__sum']
	prs_grouped_monthly = prs.annotate(month=TruncMonth('requisition_date')).values('month').annotate(uptake_rate=Sum('value') / total_budget).order_by('month')
	monthly_uptake_rate = [prs.filter(requisition_date__lt=next_month(rate['month'])).aggregate(Sum('value'))[
		'value__sum'] /
		total_budget if
		prs.filter(requisition_date__lt=next_month(rate['month'])).count() else 0 for rate in
		prs_grouped_monthly]

	monthly_conversion_rate = [pos.filter(order_date__lt=next_month(rate['month'])).aggregate(
	Sum('gross_order_value'))['gross_order_value__sum'] / prs.filter(
	requisition_date__lt=next_month(rate['month'])).aggregate(Sum('value'))[
	'value__sum'] if pos.filter(
	order_date__lt=next_month(rate['month'])).count() and
	prs.filter(requisition_date__lt=next_month(rate['month'])).count() else 0
	for rate in prs_grouped_monthly]

	uptake_conversion_data = {
	'months': [f"{calendar.month_abbr[rate['month'].month]}-{rate['month'].year}" for rate in prs_grouped_monthly],
	'uptake_rates': [round(float(rate * 100), 2) for rate in monthly_uptake_rate],
	'conversion_rates': [round(float(rate * 100), 2) for rate in monthly_conversion_rate]
	}

	uptake_conversion_data = json.dumps(uptake_conversion_data)

	supplier_categories = [supplier for supplier in Supplier.objects.distinct('category') if delivered_pos.filter(supplier__category=supplier.category).exists()]
	supplier_groups = [
	    f"{supplier.get_category_display()}-{round(delivered_pos.filter(supplier__category=supplier.category).aggregate(Sum('paid_amount'))['paid_amount__sum'] / delivered_pos.aggregate(Sum('paid_amount'))['paid_amount__sum'] * 100, 2)}%"
	    for supplier in supplier_categories]
	supplier_category_values = [
	    float(delivered_pos.filter(supplier__category=supplier.category).aggregate(Sum('paid_amount'))['paid_amount__sum'])
	    for
	    supplier in
	    supplier_categories]
	supplier_data = dict(zip(supplier_groups, supplier_category_values)) 
	supplier_data = json.dumps(supplier_data)


	context = {
		'title': 'Home',
		'order_summaries': {
			'Total Orders': pos.count(),
			'Delivered Orders': delivered_pos.count(),
			'Delayed Orders': delayed_orders.count()
		},
		'performance_summaries': {
			'Overall Performance': percentage_format(overall_perfomance_score),
			'Overall Time Score': percentage_format(overall_time_score),
			'Overall Quantity Score': percentage_format(overall_quantity_score), 

		},
		'finance_summaries': {
			'Total Order Value': intcomma(intword(total_order_value)),
			'Invoiced': intcomma(intword(sum_of_invoiced_amounts)),
			'Paid': intcomma(intword(sum_of_paid_amounts)),
		},
		'financial_year': financial_year,
		'uptake_conversion_data': uptake_conversion_data,
		'supplier_data': supplier_data,
		'pdf_url': request.resolver_match.url_name+"-pdf",
	}
	return render(request, 'users/dashboard/home.html', context)

class CompanyRegistrationView(LoginRequiredMixin, CompanyExistsMixin, SessionWizardView):
	form_list = [
		('general_info', CompanyGeneralInfoForm),
		('team', TeamMembersFormSet),
	]

	TEMPLATES = {
		'general_info': 'users/forms/wizards/general_info.html',
		'team': 'users/forms/wizards/add_team.html',
	}

	
	def get_template_names(self):
		return [self.TEMPLATES[self.steps.current]]

	def get_form(self, step=None, data=None, files=None):
		form = super().get_form(step, data, files)

		if step is None:
			step = self.steps.current

		return form

	def done(self, form_list, **kwargs):
		general_info_cleaned_data = self.get_cleaned_data_for_step('general_info')
		team_cleaned_data = self.get_cleaned_data_for_step('team')

		# Create Company
		company = Company(**general_info_cleaned_data)
		company.save()
		self.request.user.company = company
		self.request.user.save()

		# Add Company Teams
		add_bulk_users(team_cleaned_data, company)

		messages.success(self.request, 'Your workspace has been set up')
		return redirect('home')

@login_required
@user_is_company_admin_required
def add_team(request):
	if request.method == 'POST':
		formset = TeamMembersFormSet(request.POST)
		if formset.is_valid():
			for form in formset:
				first_name = form.cleaned_data.get('first_name')
				last_name = form.cleaned_data.get('last_name')
				email = form.cleaned_data.get('email')

				user = User(
					first_name=first_name,
					last_name=last_name,
					email=email,
					company=request.user.company
					)
				password = User.objects.make_random_password()
				user.set_password(password)
				user.rand_pass = password
				user.save()

			messages.success(request, 'Team added successfully')
			return redirect(request.META.get('HTTP_REFERER', 'home'))
	else:
		formset = TeamMembersFormSet()
	context = {
		'formset': formset,
		'title': 'Team Management',
	}

	return render(request, 'users/forms/add_members.html', context)

class TeamUserUpdateView(LoginRequiredMixin, BSModalUpdateView):
	model = User
	template_name = 'users/forms/modals/update_team_user.html'
	form_class = TeamUserUpdateForm
	success_message = 'Success: User details updated'
	success_url = reverse_lazy('team-management')

class TeamUserDeleteView(LoginRequiredMixin, BSModalDeleteView):
	model = User
	template_name = 'users/forms/modals/delete_team_user.html'
	success_message = 'Success: User deleted'
	success_url = reverse_lazy('team-management')

@login_required
def profile(request):
	"""
	This view loads the Profile Details of the given user making the request

	The request for this view is `users/team/profile/`

	"""
	form = UserProfileUpdateForm(request.POST or None, instance=request.user)

	if form.is_valid():
		form.save()
		messages.success(request, 'Profile Updated successfully')

		return redirect(request.META.get('HTTP_REFERER', 'home'))

	context = {
		'title': 'User Profile',
		'form': form
	}

	return render(request, 'users/forms/profile.html', context)