from django.shortcuts import redirect

class CompanyExistsMixin:
    def dispatch(self, request, *args, **kwargs):
    	if request.user.company is not None:
    		return redirect('home')
    	else:
    		return super().dispatch(request, *args, **kwargs)