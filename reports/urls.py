from django.urls import path

from .views import (
		upload_progress, upload_budget, clear_filters,
		budget_uptake, budget_spend,
		pr_tracking, po_tracking, po_issued,
        performance_pr, performance_po, payments,
        pr_item_details, po_item_details,
	)

from .pdf import views as pdf_views
from .optimization import views as serverside_views

urlpatterns = [
    path('home/pdf/', pdf_views.home, name='home-pdf'),

    path('upload/progress/', upload_progress, name='upload-progress'),
    path('upload/budget/', upload_budget, name='upload-budget'),
    path('clear_filters/', clear_filters, name='clear_filters'),

    path('budget/uptake/', budget_uptake, name='budget-uptake'),
    path('budget/uptake/pdf/', pdf_views.budget_uptake, name='budget-uptake-pdf'),

    path('budget/spend/', budget_spend, name='budget-spend'),
    path('budget/spend/pdf/', pdf_views.budget_spend, name='budget-spend-pdf'),

    path('pr-tracking/', pr_tracking, name='pr-tracking'),
    path('pr-tracking/serverside/', serverside_views.PRTrackingServerSideView.as_view(), name='pr-tracking-serverside'),
    path('pr-tracking/pdf/', pdf_views.pr_tracking, name='pr-tracking-pdf'),

    path('po-tracking/', po_tracking, name='po-tracking'),
    path('po-tracking/serverside/', serverside_views.POTrackingServerSideView.as_view(), name='po-tracking-serverside'),
    path('po-tracking/pdf/', pdf_views.po_tracking, name='po-tracking-pdf'),

    path('po-issued/', po_issued, name='po-issued'),
    path('po-issued/serverside/', serverside_views.POIssuedServerSideView.as_view(), name='po-issued-serverside'),
    path('po-issued/pdf/', pdf_views.po_issued, name='po-issued-pdf'),

    path('performance-pr/', performance_pr, name='performance-pr'),
    path('performance-pr/serverside/', serverside_views.PerformancePRServerSideView.as_view(), name='performance-pr-serverside'),
    path('performance-pr/pdf/', pdf_views.performance_pr, name='performance-pr-pdf'),

    path('performance-po/', performance_po, name='performance-po'),
    path('performance-po/serverside/', serverside_views.PerformancePOServerSideView.as_view(), name='performance-po-serverside'),
    path('performance-po/pdf/', pdf_views.performance_po, name='performance-po-pdf'),

    path('payments/', payments, name='payments'),
    path('payments/serverside/', serverside_views.PaymentsServerSideView.as_view(), name='payments-serverside'),
    path('payments/pdf/', pdf_views.payments, name='payments-pdf'),

    path('pr/details/<int:requisition_id>/', pr_item_details, name='pr_item_details'),

    path('po/details/<int:order_id>/', po_item_details, name='po_item_details'),
]