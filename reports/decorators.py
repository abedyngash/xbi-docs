from django.contrib import messages
from django.shortcuts import redirect

def company_required(function):
	def wrap(request, *args, **kwargs):
		if request.user.company is None:
			messages.warning(request, "Please set up your workspace")
			return redirect('company-setup')
		return function(request, *args, **kwargs)
	wrap.__doc__ = function.__doc__
	wrap.__name__ = function.__name__
	return wrap