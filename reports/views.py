import fiscalyear, json, calendar, datetime
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.contrib.humanize.templatetags.humanize import intword, intcomma
from django.db.models.functions import TruncMonth, Coalesce, ExtractDay
from django.db.models import Avg, Sum, Q, F, Max, Sum, DurationField, ExpressionWrapper

from .forms import BudgetUploadForm, ProgressUploadForm
from .tasks import process_progress_upload, process_budget_upload
from .models import Requisition, Order, Budget, Department
from .utils import percentage_format, months_between_dates, money_format, datetime_days_format, next_month
from .decorators import company_required


# Create your views here.
fiscalyear.START_MONTH = settings.FISCAL_YEAR_START_MONTH


def get_budget_graph_data(prs, pos, delivered_pos, _type):
	prs_grouped_monthly = prs.annotate(requisition_month=TruncMonth('requisition_date'))
	pos_grouped_monthly = delivered_pos.annotate(delivery_month=TruncMonth('vendor_delivery_date'))

	if pos:
		months = months_between_dates(pos.earliest('order_date').order_date, pos.latest('order_date').order_date)
	else:
		months = []

	if _type == 'uptake':
		uptake_over_months = [
			prs.filter(requisition_date__lt=next_month(month)).aggregate(value_sum=Coalesce(Sum('value'), 0))["value_sum"] for
			month in months
		]

		spend_over_months = [
			delivered_pos.filter(vendor_delivery_date__lt=next_month(month)).aggregate(paid_sum=Coalesce(Sum('paid_amount'), 0))["paid_sum"] for
			month in months
		]
	elif _type == 'spend':
		uptake_over_months = [
			prs_grouped_monthly.filter(requisition_date__lt=next_month(month)).aggregate(value_sum=Coalesce(Sum('value'), 0))["value_sum"] for
			month in months
		]

		spend_over_months = [
			pos_grouped_monthly.filter(vendor_delivery_date__lt=next_month(month)).aggregate(paid_sum=Coalesce(Sum('paid_amount'), 0))["paid_sum"] for
			month in months
		]

	budget_uptake_spend_data = {
		'months': [month.strftime("%b-%y")for month in months],
		'uptake_over_months': [float(uptake) for uptake in uptake_over_months],
		'spend_over_months': [float(spend) for spend in spend_over_months]
	}

	serialized_budget_uptake_spend_data = json.dumps(budget_uptake_spend_data)

	return serialized_budget_uptake_spend_data	

def get_po_graph_data(pos):
	if pos:
		months = months_between_dates(
			datetime.datetime(pos.order_by('order_date').first().order_date.year,
				pos.order_by('order_date').first().order_date.month, 
				1
			),
			datetime.datetime(pos.order_by('order_date').last().order_date.year,
			    pos.order_by('order_date').last().order_date.month,
			    1
			)
		)
	else:
		months = []
	pos_raised_monthly = [pos.filter(order_date__year=month.year, order_date__month=month.month).count() for month in months]
	pos_released_monthly = [pos.filter(release_date__year=month.year, release_date__month=month.month).count() for month in months]

	po_chart_data = {
		'months': [f"{calendar.month_abbr[month.month]}-{month.year}" for month in months],
		'pos_raised_monthly': pos_raised_monthly,
		'pos_released_monthly': pos_released_monthly
	}

	return po_chart_data

@login_required
@company_required
def upload_progress(request):
	if request.method == 'POST':
		form = ProgressUploadForm(request.POST, request.FILES)
		if form.is_valid():
			upload_instance = form.save(commit=False)
			upload_instance.company = request.user.company
			upload_instance.save()

			# Task goes here
			result = process_progress_upload.delay(request.user.id)
			task_id = result.task_id
			return render(request, 'reports/upload/progress_bar.html', {'task_id': task_id})
	else:
		form = ProgressUploadForm()

	context = {
		'title': 'Uploads',
		'form': form
	}
	return render(request, 'reports/upload/upload_progress.html', context)

@login_required
@company_required
def upload_budget(request):
	if request.method == 'POST':
		form = BudgetUploadForm(request.POST, request.FILES)
		if form.is_valid():
			upload_instance = form.save(commit=False)
			upload_instance.company = request.user.company
			upload_instance.save()

			# Task goes here
			result = process_budget_upload.delay(request.user.id)
			task_id = result.task_id
			return render(request, 'reports/upload/progress_bar.html', {'task_id': task_id})
	else:
		form = BudgetUploadForm()

	context = {
		'title': 'Uploads',
		'form': form
	}
	return render(request, 'reports/upload/upload_budget.html', context)

@login_required
@company_required
def clear_filters(request):
	url = request.META.get('HTTP_REFERER')
	cleaned_url = str(url).split('?')[0]
	return redirect(cleaned_url)

@login_required
@company_required
def budget_uptake(request):
	prs = Requisition.objects.all().filter(company=request.user.company)
	pos = Order.objects.all().filter(company=request.user.company)
	delivered_pos = Order.objects.filter(company=request.user.company).exclude(vendor_delivery_date=None)
	year = Budget.objects.filter(company=request.user.company).aggregate(Max('fiscal_year'))['fiscal_year__max'] or datetime.date.today().year
	budgets = Budget.objects.filter(company=request.user.company, fiscal_year=year)
	departments = Department.objects.all().filter(company=request.user.company)
	rows = []

	total_budget = budgets.aggregate(Sum('amount'))['amount__sum'] or 1
	total_pr_value = prs.aggregate(Sum('value'))["value__sum"] or 1
	total_po_value = pos.aggregate(Sum('gross_order_value'))["gross_order_value__sum"] or 1
	overall_uptake_rate = percentage_format(total_pr_value / total_budget) or percentage_format(1) 
	conversion_rate = percentage_format(total_po_value / total_pr_value) or percentage_format(1)

	financial_year = f'{fiscalyear.FiscalYear(int(year)).start.year}-{fiscalyear.FiscalYear(int(year)).end.year}'

	for department in departments:
		if budgets.filter(department=department).exists():
			department_budget = department.budgets.latest('id').amount
			department_budget_percentage = department_budget / total_budget
			department_pr_value = prs.filter(department=department).aggregate(Sum('value'))["value__sum"] or 1
			department_uptake_rate = department_pr_value / department.budgets.latest('id').amount or 1
			department_spend = pos.filter(department=department).aggregate(Sum('paid_amount'))["paid_amount__sum"] or 1
			department_spend_rate = department_spend / department.budgets.latest('id').amount or 1
			row = [
			    department.name,
			    money_format(department.budgets.latest('id').amount),
			    percentage_format(department_budget_percentage),
			    money_format(department_pr_value),
			    prs.filter(department=department).count(),
			    percentage_format(department_uptake_rate)
			]
			rows.append(row)
	department_ids = [department.id for department in departments]
	rows = dict(zip(department_ids, rows))

	context = {
		'title' : 'Budget Uptake',
		'pdf_url': request.resolver_match.url_name+"-pdf",
		'summaries': {
			f'Budget FY : {financial_year}': intword(total_budget),
			'Uptake': intcomma(intword(total_pr_value)),
			'Uptake Rate': overall_uptake_rate,
			'Conversion Rate': conversion_rate
		},
		'budget_uptake_spend_data': get_budget_graph_data(prs, pos, delivered_pos, 'uptake'),
		'financial_year': financial_year,
		'table': {
			'title': 'Budget',
            'columns': ['Department', 'Budget', '% Allocation', 'Uptake (KES)', 'Total Requisitions', 'Uptake Rate', ''],
            'rows': rows
        },
        'rows': rows,
		'type': 'uptake',
	}

	return render(request, 'reports/lists/budget.html', context)

@login_required
@company_required
def budget_spend(request):
	prs = Requisition.objects.all().filter(company=request.user.company)
	pos = Order.objects.all().filter(company=request.user.company)
	released_pos = Order.objects.filter(company=request.user.company).exclude(release_date=None)
	delivered_pos = Order.objects.filter(company=request.user.company).exclude(vendor_delivery_date=None)
	year = Budget.objects.filter(company=request.user.company).aggregate(Max('fiscal_year'))['fiscal_year__max'] or datetime.date.today().year
	budgets = Budget.objects.filter(company=request.user.company).filter(fiscal_year=year)
	departments = Department.objects.filter(company=request.user.company)
	rows = []

	total_budget = budgets.aggregate(Sum('amount'))['amount__sum'] or 1
	spend_total = delivered_pos.aggregate(spend_sum=Coalesce(Sum('paid_amount'), 0))["spend_sum"] or 1
	total_pr_value = prs.aggregate(Sum('value'))["value__sum"] or 1
	total_po_value = pos.aggregate(Sum('gross_order_value'))["gross_order_value__sum"] or 1

	total_approved_po_count = released_pos.count() or 1
	total_po_count = pos.count() or 1

	po_issue_rate = percentage_format(total_approved_po_count / total_po_count) or percentage_format(1)

	financial_year = f'{fiscalyear.FiscalYear(int(year)).start.year}-{fiscalyear.FiscalYear(int(year)).end.year}'
	 	
	for department in departments:
	    if Budget.objects.filter(department=department).exists():
	        department_pos = pos.filter(department=department)
	        department_prs = prs.filter(department=department)
	        department_pos_value = department_pos.aggregate(po_sum_value=Coalesce(Sum('gross_order_value'), 0))["po_sum_value"] or 1
	        department_prs_value = department_prs.aggregate(pr_sum_value=Coalesce(Sum('value'), 0))["pr_sum_value"] or 1
	        department_wip = department_prs_value - department_pos_value
	        department_budget = department.budgets.latest('id').amount or 1
	        department_pr_value = Requisition.objects.filter(company=request.user.company, department=department).aggregate(Sum('value'))["value__sum"] or 1
	        department_uptake_rate = department_pr_value / department.budgets.latest('id').amount or 1
	        department_spend = delivered_pos.filter(department=department).aggregate(Sum('paid_amount'))["paid_amount__sum"] or 1
	        department_spend_rate = department_spend / department.budgets.latest('id').amount or 1
	        department_po_count = department_pos.count() or 1
	        department_approved_po_count = department_pos.exclude(release_date=None).count() or 1
	        department_po_issue_rate = percentage_format(department_approved_po_count / department_po_count) or 1
	        row = [
	            department.name,
	            money_format(department.budgets.latest('id').amount),
	            money_format(department_spend),
	            money_format(department_wip),
	            department_pos.count,
	            department_po_issue_rate,
	        ]
	        rows.append(row)
	dept_ids = [dept.id for dept in departments if dept.budgets]
	rows = dict(zip(dept_ids, rows))

	context = {
		'title' : 'Budget Spend',
		'pdf_url': request.resolver_match.url_name+"-pdf",
		'summaries': {
			f'Budget FY : {financial_year}': intword(total_budget),
	        "Current Actual Spend": intcomma(intword(spend_total)),
	        "Total WIP": money_format(total_pr_value - total_po_value),
            "Order Issue Rate": po_issue_rate,
        },
        'budget_uptake_spend_data': get_budget_graph_data(prs, pos, delivered_pos, 'spend'),
		'financial_year': financial_year,
		'table': {
			'title': 'Budget',
            'columns': ['Department', 'Budget', 'Current Actual Spend (KES)', 'WIP (KES)','Total Orders Raised', 'Orders Issue Rate', ""],
            'rows': rows
        },
        'rows': rows,
		'type': 'spend'
	}

	return render(request, 'reports/lists/budget.html', context)

@login_required
@company_required
def pr_tracking(request):
	prs = Requisition.objects.all().filter(company=request.user.company)
	released_prs = Requisition.objects.filter(company=request.user.company).exclude(release_date=None)

	# pr_avg_processing_time = datetime_days_format(released_prs.aggregate(
 #            average_difference=Avg(F('release_date') - F('requisition_date')))['average_difference']) or 1

	if prs:
		months = months_between_dates(
			datetime.datetime(prs.order_by('requisition_date').first().requisition_date.year,
				prs.order_by('requisition_date').first().requisition_date.month, 
				1
			),
			datetime.datetime(prs.order_by('requisition_date').last().requisition_date.year,
			    prs.order_by('requisition_date').last().requisition_date.month,
			    1
			)
		)
	else:
		months = []
	
	prs_raised_monthly = [prs.filter(requisition_date__year=month.year, requisition_date__month=month.month).count() for month in months]
	prs_released_monthly = [prs.filter(release_date__year=month.year, release_date__month=month.month).count() for month in months]

	pr_chart_data = {
		'months': [f"{calendar.month_abbr[month.month]}-{month.year}" for month in months],
		'prs_raised_monthly': prs_raised_monthly,
		'prs_released_monthly': prs_released_monthly
	}

	context = {
		'title' : 'Requisition Tracking',
		'pdf_url': request.resolver_match.url_name+"-pdf",
		'summaries' : {
	        "Value of Requisitions Raised": money_format(prs.aggregate(value_sum=Coalesce(Sum('value'), 0))["value_sum"]) or 1,
	        "Number of Requisitions Raised": prs.count(),
	        "Value of Requisitions Released": money_format(released_prs.aggregate(value_sum=Coalesce(Sum('value'), 0))["value_sum"]) or 1,
	        "Number of Requisitions Released": released_prs.count(),
	    },
	    'table': {
            'columns': [
                'Requisition Number',
                'Item Description',
                'Value (KES)',
                'Requisition Status',
                'Release Date',
                "Processing Days",
                "",
            ],
        },
	    'pr_chart_data' : pr_chart_data,
	}

	return render(request, 'reports/lists/pr_tracking.html', context)

@login_required
@company_required
def po_tracking(request):
	pos = Order.objects.all().filter(company=request.user.company)
	released_pos = Order.objects.filter(company=request.user.company).exclude(release_date=None)

	# pr_avg_processing_time = datetime_days_format(released_prs.aggregate(
 #            average_difference=Avg(F('release_date') - F('requisition_date')))['average_difference']) or 1

	context = {
		'title' : 'Order Tracking',
		'pdf_url': request.resolver_match.url_name+"-pdf",
		'summaries' : {
	        "Total Value of Orders Raised": money_format(pos.aggregate(value_sum=Coalesce(Sum('gross_order_value'), 0))["value_sum"]) or 1,
	        "Total Number of Orders Raised": pos.count(),
	        "Total Value of Orders Released": money_format(released_pos.aggregate(value_sum=Coalesce(Sum('gross_order_value'), 0))["value_sum"]) or 1,
	        "Number of Orders Released": released_pos.count(),
	    },
	    'table': {
            'columns': [
                'Order Number',
                'Item Description',
                'Value (KES)',
                'Order Status',
                'Release Date',
                "Processing Days",
                "",
            ],
        },
	    'po_chart_data' : get_po_graph_data(pos),
	}

	return render(request, 'reports/lists/po_tracking.html', context)

@login_required
@company_required
def po_issued(request):
	pos = Order.objects.filter(company=request.user.company).exclude(release_date=None)
	delivered_pos = pos.exclude(vendor_delivery_date=None).exclude(release_date=None).annotate(
        delivery_days=ExpressionWrapper(F('vendor_delivery_date') - F('release_date'), output_field=DurationField()))
	delivered_pos_count = delivered_pos.count() or 1
	time_scores = [90 / po.delivery_days.days if po.delivery_days > datetime.timedelta(days=90) else 1 for po in delivered_pos]
	pos_with_delivered_quantity = pos.filter(quantity_delivered__gt=0).annotate(grn_diff=F('quantity_delivered') - F('order_quantity'))
	overall_time_score = sum(time_scores) / delivered_pos_count or 1
	overall_quantity_score = sum([po.quantity_delivered / po.order_quantity if po.grn_diff < 1 else 1 for po in
                                  pos_with_delivered_quantity]) / pos_with_delivered_quantity.count() if pos_with_delivered_quantity.count() else 0
	po_avg_delivery_time = datetime_days_format(
        pos.aggregate(average_difference=Avg(F('vendor_delivery_date') - F('release_date')))[
            'average_difference'])

	context = {
		'title' : 'Oders Issued',
		'pdf_url': request.resolver_match.url_name+"-pdf",
		'summaries' : {
	        "Total Value of Orders Issued": money_format(pos.aggregate(value_sum=Coalesce(Sum('gross_order_value'), 0))["value_sum"]),
	        "Number of Orders Issued": pos.count(),
	        "Average Delivery Time": po_avg_delivery_time,
	        "Overall Time Score": percentage_format(overall_time_score),
	        "Overall Quantity Score": percentage_format(overall_quantity_score),
	        "Overall Performance Score": percentage_format((overall_time_score + overall_quantity_score) / 2),
	    },
	    'table': {
            'columns': [
                'Order Number',
                'Supplier',
                'Item Description',
                'Qty',
                'Value',
                'Delivery Status',
                "Expected Delivery Date",
                "Delivery Days",
                "",
            ],
        },
        'po_chart_data': get_po_graph_data(pos)
	}

	return render(request, 'reports/lists/po_issued.html', context)

@login_required
@company_required
def performance_pr(request):
	raised_prs = Requisition.objects.filter(company=request.user.company)
	processed_prs = Requisition.objects.filter(company=request.user.company).exclude(release_date=None)

	pr_avg_processing_time = datetime_days_format(processed_prs.aggregate(
        average_difference=Avg(F('release_date') - F('requisition_date')))['average_difference'])
	timely_prs_count = processed_prs.annotate(
        diff=ExpressionWrapper(F('release_date') - F('requisition_date'), output_field=DurationField())
    ).filter(diff__lte=datetime.timedelta(5)).count() or 1
	processed_prs_count = processed_prs.exclude(release_date=None).count() or 1
	pr_efficiency_score = percentage_format(timely_prs_count / processed_prs_count) or '0%'

	if processed_prs:
        # Chart Data
		months = months_between_dates(datetime.datetime(processed_prs.order_by('requisition_date').first().requisition_date.year,
		                                                processed_prs.order_by('requisition_date').first().requisition_date.month,
		                                                1),
		                              datetime.datetime(processed_prs.order_by('requisition_date').last().requisition_date.year,
		                                                processed_prs.order_by('requisition_date').last().requisition_date.month,
		                                                1))
		efficiency_score_per_manager = [
		    (processed_prs.filter(release_date__month=month.month, release_date__year=month.year).annotate(
		        diff=ExpressionWrapper(F('release_date') - F('requisition_date'), output_field=DurationField())
		    ).filter(diff__lte=datetime.timedelta(5)).count() / processed_prs.filter(release_date__month=month.month,
		                                                                   release_date__year=month.year).count())
		    if processed_prs.filter(release_date__month=month.month, release_date__year=month.year).exists() else 0 for month in
		    months]
		performance_chart_data = {
		    'months': [f"{calendar.month_abbr[month.month]}-{month.year}" for month in months],
		    'efficiency_score_per_manager': [round(score * 100, 2) for score in efficiency_score_per_manager],
		}
	else:
		performance_chart_data = {'months': [], 'efficiency_score_per_manager': []}        

	context = {
		'title' : 'Requisition Performance',
		'pdf_url': request.resolver_match.url_name+"-pdf",
		'summaries' : {
			"Total Value of Requisitions": money_format(
		    	raised_prs.aggregate(sum_value=Coalesce(Sum('value'), 0))["sum_value"]),
		    "Total Number of Requisitions": raised_prs.count(),
		    "Average Processing Time": pr_avg_processing_time,
		    "Efficiency Score": pr_efficiency_score,
		},
		'table': {
			'columns': [
				'Requisition Number', 
				'Department',
                'Value',
                'Requisition Date',
                'Processing Status',
                'Release Date',
                'Duration'
            ],
        },
		'performance_chart_data': performance_chart_data
	}

	return render(request, 'reports/lists/performance_pr.html', context)

@login_required
@company_required
def performance_po(request):
	pos = Order.objects.all().filter(company=request.user.company)
	processed_pos = Order.objects.filter(company=request.user.company).exclude(release_date=None)

	po_avg_processing_time = datetime_days_format(
        pos.aggregate(average_difference=Avg(F('release_date') - F('order_date')))['average_difference'])
	timely_pos_count = pos.annotate(
        diff=ExpressionWrapper(F('release_date') - F('order_date'), output_field=DurationField())
    ).filter(diff__lte=datetime.timedelta(5)).count() or 1
	processed_pos_count = pos.exclude(release_date=None).count() or 1
	po_efficiency_score = percentage_format(timely_pos_count / processed_pos_count) or "-"

	if pos.exists():
	    # Chart Data
	    months = months_between_dates(datetime.datetime(pos.order_by('order_date').first().order_date.year,
	                                                    pos.order_by('order_date').first().order_date.month,
	                                                    1),
	                                  datetime.datetime(pos.order_by('order_date').last().order_date.year,
	                                                    pos.order_by('order_date').last().order_date.month,
	                                                    1))
	    efficiency_score_per_manager = [
	        (pos.filter(release_date__month=month.month, release_date__year=month.year).annotate(
	            diff=ExpressionWrapper(F('release_date') - F('order_date'), output_field=DurationField())
	        ).filter(diff__lte=datetime.timedelta(5)).count() / pos.filter(release_date__month=month.month,
	                                                                       release_date__year=month.year).count())
	        if pos.filter(release_date__month=month.month, release_date__year=month.year).exists() else 0
	        for month in months]
	    performance_chart_data = {
	        'months': [f"{calendar.month_abbr[month.month]}-{month.year}" for month in months],
	        'efficiency_score_per_manager': [score * 100 for score in efficiency_score_per_manager],
	    }
	else:
		performance_chart_data = {'months': [], 'efficiency_score_per_manager': []}

	context = {
		'title' : 'Order Performance',
		'pdf_url': request.resolver_match.url_name+"-pdf",
		'summaries' : {
			"Total Value of Orders": money_format(
	        	pos.aggregate(sum_value=Coalesce(Sum('gross_order_value'), 0))["sum_value"]),
	        "Total No. of Orders": pos.count(),
	        "Avg. Processing Time": po_avg_processing_time,
	    	"Efficiency Score": po_efficiency_score,
		},
		'table': {
			'columns': [
				'Order Number', 
				'Department',
	            'Value',
	            'Processing Status',
	            'Release Date',
	            'Duration'
	        ],
	    },
	    'performance_chart_data' : performance_chart_data
	}

	return render(request, 'reports/lists/performance_po.html', context)    	

@login_required
@company_required
def payments(request):
	pos = Order.objects.all().filter(company=request.user.company)

	sum_of_paid_amounts = pos.aggregate(Sum('paid_amount'))["paid_amount__sum"] or 1
	sum_of_invoiced_amounts = pos.aggregate(Sum('invoiced_amount'))["invoiced_amount__sum"] or 1
	payment_rate = sum_of_paid_amounts / sum_of_invoiced_amounts or 1
	invoicing_rate = percentage_format(pos.exclude(invoiced_amount=0).count() / pos.count()) if pos.count() else "0%"
	total_amount_paid = pos.aggregate(paid_total=Coalesce(Sum('paid_amount'), 0))["paid_total"] or 1
	total_amount_invoiced = pos.aggregate(invoice_total=Coalesce(Sum('invoiced_amount'), 0))["invoice_total"] or 1

	if pos.exists():
	    # Chart Data
	    months = months_between_dates(datetime.datetime(pos.order_by('order_date').first().order_date.year,
	                                                    pos.order_by('order_date').first().order_date.month, 1),
	                                  datetime.datetime(pos.order_by('order_date').last().order_date.year,
	                                                    pos.order_by('order_date').last().order_date.month, 1))
	    pos_delivered_monthly = [
	        pos.annotate(delivery_month=TruncMonth('vendor_delivery_date')).filter(delivery_month=month).count() for
	        month in months]
	    pos_paid_monthly = [
	        pos.exclude(paid_amount=None).annotate(payment_month=TruncMonth('vendor_delivery_date')).filter(
	            payment_month=month).count() for month in months]
	    payment_chart_data = {
	        'months': [f"{calendar.month_abbr[month.month]}-{month.year}" for month in months],
	        'pos_delivered_monthly': pos_delivered_monthly,
	        'pos_paid_monthly': pos_paid_monthly
	    }
	else:
		payment_chart_data = {'months': [], 'pos_delivered_monthly':[], 'pos_paid_monthly':[]}

	context = {
		'title' : 'Payments',
		'pdf_url': request.resolver_match.url_name+"-pdf",
		'summaries' : {
			"Orders Delivered": pos.exclude(vendor_delivery_date=None).count(),
	        "Total Invoices (KES)": money_format(total_amount_invoiced),
	        "Total Payments (KES)": money_format(total_amount_paid),
	        "Payment Rate": percentage_format(payment_rate),
		},
		'table': {'title': 'Payments',
          	'columns': [
              	'Order Number',
                'Supplier Name',
                'Supplier Group',
                'Payment Status',
                'Invoiced Amount',
                'Amount Paid',
                ''
            ]
        },
        'payment_chart_data' : payment_chart_data
	}

	return render(request, 'reports/lists/payments.html', context)

@login_required
@company_required
def pr_item_details(request, requisition_id):
    context = {
        'pr': Requisition.objects.get(id=requisition_id),
        'pdf_url': request.resolver_match.url_name+"-pdf",
    }

    return render(request, 'reports/details/pr_item_details.html', context)

@login_required
@company_required
def po_item_details(request, order_id):
    context = {
        'po': Order.objects.get(id=order_id),
        'pdf_url': request.resolver_match.url_name+"-pdf",
    }

    return render(request, 'reports/details/po_item_details.html', context)