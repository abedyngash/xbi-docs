from django.contrib import admin
from .models import ProgressUpload, BudgetUpload, Department, Budget, Requisition, Order
# Register your models here.

admin.site.register(ProgressUpload)
admin.site.register(BudgetUpload)
admin.site.register(Department)
admin.site.register(Budget)
admin.site.register(Requisition)
admin.site.register(Order)