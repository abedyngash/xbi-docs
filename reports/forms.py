from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column, Field, Div, ButtonHolder, HTML

from .models import ProgressUpload, BudgetUpload

class ProgressUploadForm(forms.ModelForm):
	progress_file = forms.FileField(widget=forms.FileInput(attrs={'class': 'form-control'}))
	class Meta:
		model = ProgressUpload
		fields = ['progress_file']
		
class BudgetUploadForm(forms.ModelForm):
	budget_file = forms.FileField(widget=forms.FileInput(attrs={'class': 'form-control'}))
	class Meta:
		model = BudgetUpload
		fields = ['budget_file']

	def __init__(self, *args, **kwargs):
	    super(BudgetUploadForm, self).__init__(*args, **kwargs)
	    self.helper = FormHelper()
	    self.helper.form_tag = False
	    self.helper.layout = Layout(
	    	Row(
				Column(
					Field('budget_file', css_id="", css_class=""),
    			)
	    	)
	    )

class UploadWizardForm(forms.Form):
	progress_work_book = forms.FileField(widget=forms.FileInput(attrs={'class': 'form-control'}))
	budget_work_book = forms.FileField(widget=forms.FileInput(attrs={'class': 'form-control'}))

	def __init__(self, *args, **kwargs):
	    super(UploadWizardForm, self).__init__(*args, **kwargs)
	    self.helper = FormHelper()
	    self.helper.form_tag = False
	    self.helper.layout = Layout(
	    	Row(
				Column(
					Field('budget_work_book', css_id="", css_class=""),
    			),
				Column(
					Field('progress_work_book', css_id="", css_class=""),
    			),
	    	),
	    	ButtonHolder(
		    	HTML(
		    		'{% include "users/forms/wizards/_buttons.html" %}'
		    	)
	    	)
	    )
