from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.
CURRENCIES = (
    ('KES', 'KES'),
    ('USD', 'USD'),
    ('GBP', 'GBP'),
    ('EUR', 'EUR'),
)

SUPPLIER_CATEGORY_CHOICES = (
    ('ZFTV', 'Foreign suppliers'),
    ('ZWGV', 'Local Women suppliers'),
    ('ZYGV', 'Local Youth suppliers'),
    ('ZPWC', 'Local PWD suppliers'),
    ('ZLTV', 'Other Local suppliers'),
)

PROCUREMENT_METHOD_CHOICES = (
    ('Competitive Negotiations', 'Competitive Negotiations'),
    ('Direct Procurement', 'Direct Procurement'),
    ('Electronic Reverse Auction', 'Electronic Reverse Auction'),
    ('Framework Agreements', 'Framework Agreements'),
    ('Open Tender', 'Open Tender'),
    ('Govt to Govt', 'Govt. to Govt.'),
    ('Request For Quotation', 'Request For Quotation'),
    ('Request For Proposal', 'Request For Proposal'),
    ('Restricted Tendering', 'Restricted Tendering'),
    ('Low-Value Procurement', 'Low-Value Procurement'),
    ('Other', 'Other'),
)

def company_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/documents/company_<id>/<filename>
    if instance.company:
    	return 'documents/company_{0}/{1}/'.format(instance.company.id, filename)
    return f'documents/company_tests/{filename}/'

class BaseModel(models.Model):
	company = models.ForeignKey('users.Company', on_delete=models.CASCADE, null=True)
	created_when = models.DateTimeField(auto_now_add=True)
	last_modified = models.DateTimeField(auto_now=True)

	class Meta:
		abstract = True

class Upload(BaseModel):
	processed = models.BooleanField(default=False)

	class Meta:
		abstract = True

class BudgetUpload(Upload):
	budget_file = models.FileField(upload_to=company_directory_path)

class ProgressUpload(Upload):
	progress_file = models.FileField(upload_to=company_directory_path)

class Department(BaseModel):
	name = models.CharField(max_length=120)

	class Meta:
		db_table = 'departments'

	def __str__(self):
		return f'{self.name}'

class Budget(BaseModel):
	department = models.ForeignKey(Department, on_delete=models.CASCADE, related_name='budgets')
	amount = models.DecimalField(max_digits=20, decimal_places=2)
	currency = models.CharField(max_length=4, choices=CURRENCIES, default='KES')
	fiscal_year = models.PositiveSmallIntegerField(validators=[
        MinValueValidator(1600),
        MaxValueValidator(9999)
    ])

	class Meta:
		db_table = 'budgets'

class Supplier(BaseModel):
	name = models.CharField(max_length=255)
	category = models.CharField(max_length=255, choices=SUPPLIER_CATEGORY_CHOICES, null=True)

	class Meta:
		db_table = 'suppliers'

class Milestone(BaseModel):
	name = models.CharField(max_length=200, null=True)
	descripition = models.TextField(null=True)
	agent = models.CharField(max_length=120)
	release_date = models.DateField()

	class Meta:
		abstract = True

class Requisition(BaseModel):
	department = models.ForeignKey(Department, on_delete=models.CASCADE, null=True, related_name='requisitions')

	requisition_number = models.CharField(max_length=20, null=True)
	created_by = models.CharField(max_length=255, null=True)
	currency = models.CharField(max_length=4, choices=CURRENCIES, default='KES', null=True)
	item_description = models.CharField(max_length=255, null=True)
	item_quantity = models.IntegerField(null=True)
	status = models.CharField(max_length=50, null=True)
	value = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	procurement_method = models.CharField(max_length=100, choices=PROCUREMENT_METHOD_CHOICES, null=True)
	requisition_date = models.DateField(null=True)
	release_date = models.DateField(null=True)
	fiscal_year = models.PositiveSmallIntegerField(validators=[
        MinValueValidator(1600),
        MaxValueValidator(9999)
    ], null=True)

	class Meta:
		db_table = 'requisitions'
		unique_together = ['company', 'requisition_number', 'item_description', 'item_quantity', 'requisition_date']

	@property
	def processing_days(self):
	    "Returns processing time in days if processed"
	    if self.release_date and self.requisition_date:
	        return (self.release_date - self.requisition_date).days
	    else:
	        return (datetime.datetime.now().date() - self.requisition_date).days

	@property
	def processing_status(self):
	    if self.release_date:
	        return "Processed"
	    else:
	        return "Not Processed"

class Order(BaseModel):
	department = models.ForeignKey(Department, on_delete=models.CASCADE, null=True, related_name='orders')
	requisition = models.OneToOneField(Requisition, on_delete=models.CASCADE, null=True)
	supplier = models.ForeignKey(Supplier, on_delete=models.SET_NULL, null=True, related_name='orders')

	order_number = models.CharField(max_length=20, null=True)
	order_quantity = models.IntegerField(null=True)
	quantity_delivered = models.IntegerField(null=True)
	currency = models.CharField(max_length=4, choices=CURRENCIES, default='KES', null=True)
	gross_order_value = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	invoiced_amount = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	paid_amount = models.DecimalField(max_digits=20, decimal_places=2, null=True)
	status = models.CharField(max_length=50, null=True)
	fiscal_year = models.PositiveSmallIntegerField(validators=[
		MinValueValidator(1600),
		MaxValueValidator(9999)
	], null=True)
	order_date = models.DateField(null=True)
	release_date = models.DateField(null=True)
	vendor_delivery_date = models.DateField(null=True)
	po_delivery_date = models.DateField(null=True)

	class Meta:
		db_table = 'orders'
		unique_together = ['company', 'order_number', 'requisition', 'supplier']

	@property
	def processing_days(self):
	    "Returns processing time in days if processed"
	    if self.release_date and self.order_date:
	        return (self.release_date - self.order_date).days
	    elif self.order_date:
	        return (datetime.datetime.now().date() - self.order_date).days
	    else:
	        None

	@property
	def processing_status(self):
	    if self.release_date:
	        return "Processed"
	    else:
	    	return "Not Processed"

	@property
	def delivery_time(self):
	    if self.vendor_delivery_date and self.release_date:
	        return (self.vendor_delivery_date - self.release_date).days
	    else:
	        return None

	@property
	def delivery_status(self):
	    if self.vendor_delivery_date:
	        if self.release_date and self.delivery_time > 90:
	            return "Delivered Late"
	        elif self.release_date and self.delivery_time <= 90:
	            return "Delivered On-time"
	        else:
	            return "Delivered"
	    elif self.release_date:
	        if (datetime.date.today() - self.release_date).days > 90:
	            return "Late"
	        else:
	            return "On-going Delivery"
	    else:
	        return None

	@property
	def payment_status(self):
	    if self.paid_amount:
	        return "Paid"
	    elif self.invoiced_amount:
	        return "Due for payment"
	    else:
	        return "Pending Invoice"

class PRMilestone(Milestone):
	requisition = models.ForeignKey(Requisition, on_delete=models.CASCADE, null=True, related_name='milestones')

class POMilestone(Milestone):
	order = models.ForeignKey(Order, on_delete=models.CASCADE, null=True, related_name='milestones')