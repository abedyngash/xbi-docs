import sweetify
import pandas as pd

from celery import shared_task
from celery_progress.backend import ProgressRecorder

from django.contrib.auth import get_user_model

from django.shortcuts import redirect

from .models import (
	BudgetUpload, ProgressUpload, 
	Department, Budget, 
	Requisition, Order, Supplier
	)

User = get_user_model()

@shared_task(bind=True)
def process_progress_upload(self, user_id):
	company = User.objects.get(pk=user_id).company

	progress_recorder = ProgressRecorder(self)
	latest_upload = ProgressUpload.objects.filter(company=company).latest('id')
	if not latest_upload.processed:
		df = pd.read_excel(latest_upload.progress_file.file, sheet_name=0)
		df.fillna('', inplace=True)
		count = 0
		for index, row in df.iterrows():
			''' REQUIISTION COLUMNS '''
			# --- FIELDS ---
			# department, requisition_number, created_by, currency, item_description, item_quantity
			# status, value, procurement_method, requisition_date, release_date, fiscal_year
			# --- ---
			# unique_together = ['requisition_number', 'item_description', 'item_quantity', 'requisition_date']

			department_name = row['Department']
			department, created = Department.objects.get_or_create(name=department_name, company=company)

			requisition_number = row['Requisition Number']
			item_description = row['Item of Requisition']
			item_quantity = row['Requisition Quantity']
			requisition_date = row['Requisition Date']

			created_by = row['Requisition Created By']
			currency = row['Currency']
			status = row['Requisition Status']
			value = row['Requisition Value']
			procurement_method = row['Procurement Method']
			release_date = row['Requisition Release Date']
			fiscal_year = row['Fiscal Year']

			requisition, created = Requisition.objects.get_or_create(
				requisition_number=requisition_number,
				item_description=item_description,
				item_quantity=item_quantity,
				requisition_date=requisition_date,
				company=company
			)
			requisition.department = department
			requisition.created_by=created_by
			requisition.currency=currency
			requisition.status=status
			requisition.value=value
			requisition.procurement_method=procurement_method
			requisition.release_date=release_date
			requisition.fiscal_year=fiscal_year

			requisition.save()


			''' ORDER COLUMNS '''
			# --- FIELDS ---
			# department, requisition, supplier, order_number, order_quantity, quantity_delivered,
			# currency, gross_order_value, invoiced_amount, paid_amount, status, fiscal_year,
			# order_date, release_date, vendor_delivery_date, po_delivery_date
			# --- ---
			# unique_together = ['order_number', 'requisition', 'supplier']

			supplier_name = row['Supplier Name']
			supplier_category = row['Supplier Group']

			supplier, created = Supplier.objects.get_or_create(
				name=supplier_name, 
				category=supplier_category
				)

			order_number = row['Order Number']

			order_quantity = row['Order Quantity']
			quantity_delivered = row['Quantity Delivered']
			currency = row['Currency']
			gross_order_value = row['Gross Order Value']
			invoiced_amount = row['Invoiced Amount']
			paid_amount = row['Paid Amount']
			po_status = row['Order Status']
			fiscal_year = row['Fiscal Year']
			order_date = row['Order Date']
			order_release_date = row['Order Release Date']
			vendor_delivery_date = row['Vendor Delivery Date']
			po_delivery_date = row['Order Delivery Date']

			if requisition:
				order, created = Order.objects.get_or_create(
					order_number = order_number,
					requisition=requisition,
					supplier=supplier,
					company=company
				)
				order.department = department
				order.order_quantity=order_quantity
				order.quantity_delivered=quantity_delivered
				order.currency=currency
				order.gross_order_value=gross_order_value
				order.invoiced_amount=invoiced_amount
				order.paid_amount=paid_amount
				order.status=po_status
				order.fiscal_year=fiscal_year
				order.order_date=order_date
				order.release_date=release_date
				order.vendor_delivery_date=vendor_delivery_date
				order.po_delivery_date=po_delivery_date

				order.save()

			progress_recorder.set_progress(index, len(df), f'Uploading record {index} of {len(df)}')
		latest_upload.processed = True
		latest_upload.delete()
		return f"Processed {count} records"
	return "No records to process"

@shared_task(bind=True)
def process_budget_upload(self, user_id):
	company = User.objects.get(pk=user_id).company

	progress_recorder = ProgressRecorder(self)
	latest_upload = BudgetUpload.objects.filter(company=company).latest('id')
	if not latest_upload.processed:
		df = pd.read_excel(latest_upload.budget_file.file, sheet_name=0)
		df.fillna('', inplace=True)
		count = 0
		for index, row in df.iterrows():
			department_name = row['Department']
			amount = float(int(row['Amount']))
			fiscal_year = row['Fiscal Year']

			department, created = Department.objects.get_or_create(name=department_name, company=company)
			Budget.objects.create(department=department, amount=amount, fiscal_year=fiscal_year, company=company)
			
			count += 1
			progress_recorder.set_progress(index, len(df), f'Uploading record {index} of {len(df)}')
		latest_upload.processed = True
		latest_upload.delete()
		return f"Processed {count} records"
	return "No records to process"


'''
# Milestones Algorithm

milestones_count = row['Milestones Level Count']
i=1

while i < milestones_count:
	if (f'PR_l{i}_name' in df.columns and f'PR_l{i}_description' in df.columns 
		and f'PR_l{i}_agent' in df.columns and f'PR_l{i}_release_date' in df.columns):
		name = row[f'PR_l{i}_name']
		desc = row[f'PR_l{i}_description']
		agent = row[f'PR_l{i}_agent']
		release_date = row[f'PR_l{i}_release_date']

		if release_date and name:
			milestone = PRMilestone.objects.create(
				requisition=requisition,
				name=name,
				description=desc,
				agent=agent,
				release_date=release_date 
			)
	i+=1
'''