import datetime
from urllib.parse import parse_qs

def money_format(money_value):
    if money_value == None:
        return 0.00
    return "{0:,}".format(round(money_value))

def percentage_format(number):
    if number is None:
        return '{:.0%}'.format(0.00)
    else:
        float_number = float(number)
        if float_number.is_integer():
            return '{:.0%}'.format(float_number)
        else:
            return '{:.2%}'.format(float_number)


def days_format(day_count):
    if day_count is None:
        return 0
    return "{0:,.1f}".format(day_count)


def datetime_days_format(datetime_object):
    if datetime_object is None:
        return "-"
    return datetime_object.days


def months_between_dates(start_date, end_date):
    months = []
    for year in range(start_date.year, end_date.year + 1):
        start_month = start_date.month if year == start_date.year else 1
        end_month = end_date.month + 1 if year == end_date.year else 13
        for month in range(start_month, end_month):
            months.append(datetime.datetime(year, month, 1))
    return months


def next_month(date):
    if date.month == 12:
        return datetime.datetime(date.year+1, 1, 1)
    else:
        return datetime.datetime(date.year, date.month+1, 1)


# def filter_query(query_string):
#     queries = parse_qs(query_string)
#     for key in queries.keys():
#         if 'manager' in queries.keys():
#             if queries['manager'][0] == 'Unassigned':
#                 prs = prs.filter(manager=None)
#                 pos = pos.filter(requisition__manager=None)
#                 delivered_pos = delivered_pos.filter(requisition__manager=None)
#             else:
#                 prs = prs.filter(manager__id=queries['manager'][0])
#                 pos = pos.filter(requisition__manager__id=queries['manager'][0])
#                 delivered_pos = delivered_pos.filter(requisition__manager__id=queries['manager'][0])
#         if 'quarter' in queries.keys():
#             year = int(queries['quarter'][0][-4:])
#             quarter = int(queries['quarter'][0][1])
#             budgets = DepartmentBudget.objects.filter(year=year)
#             if quarter == 4:
#                 prs = prs.filter(requisition_date__year=year, requisition_date__month__gte=4,
#                                  requisition_date__month__lte=6)
#                 pos = pos.filter(order_date__year=year, order_date__month__gte=4, order_date__month__lte=6)
#                 delivered_pos = delivered_pos.filter(vendor_delivery_date__year=year, vendor_delivery_date__month__gte=4, vendor_delivery_date__month__lte=6)
#             elif quarter == 3:
#                 prs = prs.filter(requisition_date__year=year, requisition_date__month__gte=1,
#                                  requisition_date__month__lte=3)
#                 pos = pos.filter(order_date__year=year, order_date__month__gte=1, order_date__month__lte=3)
#                 delivered_pos = delivered_pos.filter(vendor_delivery_date__year=year, vendor_delivery_date__month__gte=1, vendor_delivery_date__month__lte=3)
#             elif quarter == 2:
#                 prs = prs.filter(requisition_date__year=year - 1, requisition_date__month__gte=10,
#                                  requisition_date__month__lte=12)
#                 pos = pos.filter(order_date__year=year - 1, order_date__month__gte=10, order_date__month__lt=12)
#                 delivered_pos = delivered_pos.filter(vendor_delivery_date__year=year - 1, vendor_delivery_date__month__gte=10, vendor_delivery_date__month__lt=12)
#             elif quarter == 1:
#                 prs = prs.filter(requisition_date__year=year - 1, requisition_date__month__gte=7,
#                                  requisition_date__month__lte=9)
#                 pos = pos.filter(order_date__year=year - 1, order_date__month__gte=7, order_date__month__lte=9)
#                 delivered_pos = delivered_pos.filter(vendor_delivery_date__year=year - 1, vendor_delivery_date__month__gte=7, vendor_delivery_date__month__lte=9)