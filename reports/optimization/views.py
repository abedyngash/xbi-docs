import datetime
from urllib.parse import parse_qs

from django.views import View
from django.urls import reverse
from django.contrib.humanize.templatetags.humanize import intcomma

from reports.models import Requisition, Order
from .mixins import DatatablesServerSideView

class PRTrackingServerSideView(DatatablesServerSideView, View):
	model = Requisition
	columns = ['requisition_number', 'item_description', 'value', 'release_date']

	# foreign_fields = {'manager': 'manager__name'}

	searchable_columns = ['requisition_number', 'item_description', 'release_date']

	def customize_row(self, row, obj):
		row['value'] = intcomma(obj.value)
		row['processing_status'] = obj.processing_status
		row['processing_days'] = obj.processing_days
		url = reverse('pr_item_details', args=[obj.id])
		row['id_content'] = f'''
					<a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle">                
                        <i class="fas fa-ellipsis-v"></i>
                    </a>
                    <div class="dropdown-menu">
                        <a href="{url}" class="dropdown-item btn-sm">
                          <i class="fas fa-eye"></i>
                          &nbsp;
                          View More
                        </a>
                    </div>
				'''

	def get_initial_queryset(self):
		qs = super(PRTrackingServerSideView, self).get_initial_queryset()
		qs.order_by('id')
		if '?' in self.request.META['HTTP_REFERER']:
			query_url = str(self.request.META['HTTP_REFERER']).split('?')[1]
			if query_url:
				queries = parse_qs(query_url)
				# Filtering
				if 'start_date' in queries.keys():
				    qs = Requisition.objects.all().filter(requisition_date__gte=queries['start_date'][0])
				if 'end_date' in queries.keys():
				    qs = Requisition.objects.all().filter(requisition_date__lte=queries['end_date'][0])
				if 'department_id' in queries.keys():
				    qs = Requisition.objects.all().filter(department__id=queries['department_id'][0])
				if 'manager' in queries.keys():
				    if queries['manager'][0] == 'Unassigned':
				        qs = Requisition.objects.all().filter(manager=None)
				    else:
				        qs = Requisition.objects.all().filter(manager__id=queries['manager'][0])

		return qs

class POTrackingServerSideView(DatatablesServerSideView, View):
	model = Order
	columns = ['order_number', 'requisition', 'gross_order_value', 'status', 'release_date',]

	foreign_fields = {'requisition': 'requisition__requisition_number'}

	searchable_columns = ['order_number', 'requisition', 'release_date', 'processing_status']

	def customize_row(self, row, obj):
		row['value'] = intcomma(obj.gross_order_value)
		row['item_description'] = obj.requisition.item_description
		row['processing_status'] = obj.status
		row['processing_days'] = obj.processing_days
		url = reverse('po_item_details', args=[obj.id])
		row['id_content'] = f'''
					<a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle">                
                        <i class="fas fa-ellipsis-v"></i>
                    </a>
                    <div class="dropdown-menu">
                        <a href="{url}" class="dropdown-item btn-sm">
                          <i class="fas fa-eye"></i>
                          &nbsp;
                          View More
                        </a>
                    </div>
				'''

	def get_initial_queryset(self):
		qs = super(POTrackingServerSideView, self).get_initial_queryset()
		qs.order_by('id')
		if '?' in self.request.META['HTTP_REFERER']:
			query_url = str(self.request.META['HTTP_REFERER']).split('?')[1]
			if query_url:
				queries = parse_qs(query_url)
				# Filtering
				if 'start_date' in queries.keys():
				    qs = Order.objects.all().filter(order_date__gte=queries['start_date'][0])
				if 'end_date' in queries.keys():
				    qs = Order.objects.all().filter(order_date__lte=queries['end_date'][0])
				if 'department_id' in queries.keys():
				    qs = Order.objects.all().filter(department__id=queries['department_id'][0])
				if 'supplier_group' in queries.keys():
				    if queries['supplier_group'][0] == 'Unassigned':
				        qs = Order.objects.all().filter(supplier=None)
				    else:
				        qs = Order.objects.all().filter(supplier__category=queries['supplier_group'][0])
				if 'procurement_method' in queries.keys():
				    if queries['procurement_method'][0] == 'Unassigned':
				        qs = Order.objects.all().filter(procurement_method=None)
				    else:
				        qs = Order.objects.all().filter(procurement_method=queries['procurement_method'][0])

		return qs

class POIssuedServerSideView(DatatablesServerSideView, View):
	model = Order
	columns = ['order_number', 'supplier', 'requisition', 'order_quantity', 'gross_order_value', 'po_delivery_date']

	foreign_fields = {'requisition': 'requisition__item_description', 'supplier': 'supplier__name'}

	searchable_columns = ['order_number', 'item_description', 'release_date']

	def customize_row(self, row, obj):
		row['value'] = intcomma(obj.gross_order_value)
		row['delivery_status'] = obj.delivery_status
		row['delivery_time'] = obj.delivery_time
		url = reverse('po_item_details', args=[obj.id])
		row['id_content'] = f'''
					<a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle">                
                        <i class="fas fa-ellipsis-v"></i>
                    </a>
                    <div class="dropdown-menu">
                        <a href="{url}" class="dropdown-item btn-sm">
                          <i class="fas fa-eye"></i>
                          &nbsp;
                          View More
                        </a>
                    </div>
				'''

	def get_initial_queryset(self):
		qs = super(POIssuedServerSideView, self).get_initial_queryset()
		qs = Order.objects.all().exclude(release_date=None)

		if '?' in self.request.META['HTTP_REFERER']:
			query_url = str(self.request.META['HTTP_REFERER']).split('?')[1]
			if query_url:
				queries = parse_qs(query_url)
				# Filtering
				if 'start_date' in queries.keys():
				    qs = qs.filter(order_date__gte=queries['start_date'][0])
				if 'end_date' in queries.keys():
				    qs = qs.filter(order_date__lte=queries['end_date'][0])
				if 'department_id' in queries.keys():
				    qs = qs.filter(department__id=queries['department_id'][0])
				if 'supplier_group' in queries.keys():
				    if queries['supplier_group'][0] == 'Unassigned':
				        qs = qs.filter(supplier=None)
				    else:
				        qs = qs.filter(supplier__category=queries['supplier_group'][0])
				if 'procurement_method' in queries.keys():
				    if queries['procurement_method'][0] == 'Unassigned':
				        qs = qs.filter(procurement_method=None)
				    else:
				        qs = qs.filter(procurement_method=queries['procurement_method'][0])
				if 'delivery_status' in queries.keys():
					if queries['delivery_status'][0] == '0':
						qs = qs.filter(vendor_delivery_date=None)
					else:
						qs = qs.exclude(vendor_delivery_date=None)

		return qs

class PerformancePRServerSideView(DatatablesServerSideView, View):
	model = Requisition

	columns = ['requisition_number', 'department', 'value', 'requisition_date', 'release_date']

	foreign_fields = {'department': 'department__name'}

	searchable_columns = ['requisition_number', 'department', 'requisition_date']

	def customize_row(self, row, obj):
		row['value'] = intcomma(obj.value)
		row['processing_status'] = obj.processing_status
		row['processing_days'] = obj.processing_days
		
	def get_initial_queryset(self):
		qs = super(PerformancePRServerSideView, self).get_initial_queryset()

		if '?' in self.request.META['HTTP_REFERER']:
			query_url = str(self.request.META['HTTP_REFERER']).split('?')[1]
			if query_url:
				queries = parse_qs(query_url)
				# Filtering
				if 'start_date' in queries.keys():
				    qs = qs.filter(order_date__gte=queries['start_date'][0])
				if 'end_date' in queries.keys():
				    qs = qs.filter(order_date__lte=queries['end_date'][0])
				if 'department_id' in queries.keys():
				    qs = qs.filter(department__id=queries['department_id'][0])

				if 'manager' in queries.keys():
					if queries['manager'][0] == 'Unassigned':
						qs = qs.filter(manager=None)
					else:
						qs = qs.filter(manager__id=queries['manager'][0])
				if 'status' in queries.keys():
					if queries['status'][0] == '1':
					    qs = qs.filter(l1_release_date=None, l2_release_date=None, release_date=None)
					elif queries['status'][0] == '2':
					    qs = qs.exclude(l1_release_date=None).filter(l2_release_date=None, release_date=None)
					elif queries['status'][0] == '3':
					    qs = qs.exclude(l2_release_date=None).filter(release_date=None)
					else:
					    qs = qs.exclude(release_date=None)

		return qs

class PerformancePOServerSideView(DatatablesServerSideView, View):
	model = Order

	columns = ['order_number', 'department', 'gross_order_value', 'release_date']

	foreign_fields = {'department': 'department__name'}

	searchable_columns = columns

	def customize_row(self, row, obj):
		row['value'] = intcomma(obj.gross_order_value)
		row['processing_status'] = obj.status
		row['processing_days'] = str(obj.processing_days) + " days"
		row['payment_status'] = obj.payment_status
		
	def get_initial_queryset(self):
		qs = super(PerformancePOServerSideView, self).get_initial_queryset()

		if '?' in self.request.META['HTTP_REFERER']:
			query_url = str(self.request.META['HTTP_REFERER']).split('?')[1]
			if query_url:
				queries = parse_qs(query_url)

				# Filtering on queries
				if 'start_date' in queries.keys():
				    qs = qs.filter(order_date__gte=queries['start_date'][0])
				if 'end_date' in queries.keys():
				    qs = qs.filter(order_date__lte=queries['end_date'][0])
				if 'department_id' in queries.keys():
				    qs = qs.filter(department__id=queries['department_id'][0])
				if 'assigned' in queries.keys():
				    assigned_query = queries['assigned'][0]
				    qs = qs.filter(
				        Q(l1_description=assigned_query) | Q(l2_description=assigned_query) | Q(l3_description=assigned_query))
				if 'processing_status' in queries.keys():
				    processing_query = queries['processing_status'][0]
				    if processing_query == 'processed':
				        qs = qs.exclude(release_date=None)
				    elif processing_query == 'l1':
				        qs = qs.filter(l1_release_date=None, l2_release_date=None, release_date=None,
				                         order_date__gte=datetime.datetime.now() - datetime.timedelta(days=5))
				    elif processing_query == 'l1-delayed':
				        qs = qs.filter(l1_release_date=None, l2_release_date=None, release_date=None,
				                         order_date__lt=datetime.datetime.now() - datetime.timedelta(days=5))
				    elif processing_query == 'l2':
				        qs = qs.exclude(l1_release_date=None).filter(l2_release_date=None, release_date=None,
				                                                       l1_release_date__gte=datetime.datetime.now() - datetime.timedelta(
				                                                           days=5))
				    elif processing_query == 'l2-delayed':
				        qs = qs.exclude(l1_release_date=None).filter(l2_release_date=None, release_date=None,
				                                                       l1_release_date__lt=datetime.datetime.now() - datetime.timedelta(
				                                                           days=5))
				    elif processing_query == 'l3':
				        qs = qs.exclude(l1_release_date=None).exclude(l2_release_date=None).filter(release_date=None,
				                                                                                    l2_release_date__gte=datetime.datetime.now() - datetime.timedelta(
				                                                                                        days=5))
				    else:
				        qs = qs.exclude(l1_release_date=None).exclude(l2_release_date=None).filter(release_date=None,
				                                                                                     l2_release_date__lt=datetime.datetime.now() - datetime.timedelta(
				                                                                                         days=5))
		return qs

class PaymentsServerSideView(DatatablesServerSideView, View):
	model = Order

	columns = ['order_number', 'supplier', 'invoiced_amount', 'paid_amount']

	foreign_fields = {'supplier': 'supplier__name'}

	searchable_columns = columns

	def customize_row(self, row, obj):
		row['paid_amount'] = intcomma(obj.paid_amount)
		row['invoiced_amount'] = intcomma(obj.invoiced_amount)
		if obj.supplier:
			row['supplier_category'] = obj.supplier.get_category_display()
		else:
			row['supplier_category'] = ''
		row['payment_status'] = obj.payment_status
		url = reverse('po_item_details', args=[obj.id])
		row['id_content'] = f'''
					<a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle">                
                        <i class="fas fa-ellipsis-v"></i>
                    </a>
                    <div class="dropdown-menu">
                        <a href="{url}" class="dropdown-item btn-sm">
                          <i class="fas fa-eye"></i>
                          &nbsp;
                          View More
                        </a>
                    </div>
				'''
		
	def get_initial_queryset(self):
		qs = super(PaymentsServerSideView, self).get_initial_queryset()

		if '?' in self.request.META['HTTP_REFERER']:
			query_url = str(self.request.META['HTTP_REFERER']).split('?')[1]
			if query_url:
				queries = parse_qs(query_url)
				# Filtering
				if 'start_date' in queries.keys():
				    qs = qs.filter(vendor_delivery_date__gte=queries['start_date'][0])
				if 'end_date' in queries.keys():
				    qs = qs.filter(vendor_delivery_date__lte=queries['end_date'][0])
				if 'department_id' in queries.keys():
				    qs = qs.filter(department__id=queries['department_id'][0])
				if 'procurement_method' in queries.keys():
				    if queries['procurement_method'][0] == 'Unassigned':
				        qs = qs.filter(procurement_method=None)
				    qs = qs.filter(procurement_method=queries['procurement_method'][0])
				if 'supplier_group' in queries.keys():
				    if queries['supplier_group'][0] == 'Unassigned':
				        qs = qs.filter(supplier=None)
				    else:
				        qs = qs.filter(supplier__category=queries['supplier_group'][0])
				if 'payment_status' in queries.keys():
				    if queries['payment_status'][0] == 'paid':
				        qs = qs.exclude(paid_amount=0)
				    elif queries['payment_status'][0] == 'invoiced':
				        qs = qs.filter(paid_amount=0).exclude(invoiced_amount=None)
				    if queries['payment_status'][0] == 'not-invoiced':
				        qs = qs.filter(paid_amount=0, invoiced_amount=None)				

		return qs
