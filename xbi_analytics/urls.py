"""
xbi_analytics URL Configuration

The ``urlpatterns`` list routes URLs to views. For more information please see: https://docs.djangoproject.com/en/3.1/topics/http/urls/

**URL CONFIGURATIONS** ::

    urlpatterns = [

        path('admin/', admin.site.urls), 
        # ``admin/`` route redirects to the Django admin Site > https://docs.djangoproject.com/en/3.1/ref/contrib/admin/

        path('accounts/', include('allauth.urls')),
        # ``accounts/`` route redirects to `allauth` urls

        path('celery-progress/', include('celery_progress.urls')),
        # celery-progress is responsible for displaying the progress for Celery asynchronous tasks.

        path('', include('users.urls')), 
        # On no specified route, redirect to the users URLS (users.urls)

        path('reports/', include('reports.urls')),
        # ``reports/`` route redirects to the reports URLS (reports.urls)
    ]
    
    # Add media URL settings
    if settings.DEBUG:
        urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

**END OF URL CONFIGURATIONS**
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', include('users.urls')),
    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),
    path('celery-progress/', include('celery_progress.urls')),

    path('reports/', include('reports.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)