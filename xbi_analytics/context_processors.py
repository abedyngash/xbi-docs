"""
The views herein return a dictionary which is to provide globally accessible variables within the whole app.
See https://docs.djangoproject.com/en/3.1/ref/templates/api/ to know more about Django Context Processors
"""
from reports.models import Requisition, Order, Budget
from django.conf import settings

def determine_need_for_intro(request):
	"""
	This function checks whether the logged in user needs a tour within the system.

	Package used for the intro is ``Intro JS`` (https://introjs.com/)

	"""
	if request.user.is_authenticated:
		budgets = Budget.objects.filter(company=request.user.company)
		requisitions = Requisition.objects.filter(company=request.user.company)
		orders = Order.objects.filter(company=request.user.company)

		# if not request.user.last_login:
		# 	pass

		if not budgets and not requisitions and not orders:
			return {'needs_intro': 'all'}
			
		if not budgets:
			return {'needs_intro': 'budget_only'}
		elif not requisitions and not orders:
			return {'needs_intro': 'progress_only'}
		else:
			return {'needs_intro': None}
	return {'needs_intro': None}

def get_static_url_for_pdf_views(request):
	"""
	This is a simple function for setting a global context value for the STATIC_URL 
	used in Generating the PDF views

	Package used for generating PDF files is ``django-wkhtmltopdf`` (https://django-wkhtmltopdf.readthedocs.io/en/latest/)
	"""
	return {'static_url': settings.PDF_STATIC_URL}