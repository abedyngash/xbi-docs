from __future__ import absolute_import, unicode_literals

import os

from celery import Celery

""" Set the default Django settings module for the 'celery' program."""
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'xbi_analytics.settings')

app = Celery('xbi_analytics')
""" 
Declare Celery App ::

	app.config_from_object('django.conf:settings', namespace='CELERY')

	# Using a string here means the worker doesn't have to serialize 
	# the configuration object to child processes.

	# ``namespace='CELERY'`` means all celery-related configuration keys
	# should have a `CELERY_` prefix.
"""

app.config_from_object('django.conf:settings', namespace='CELERY')


app.autodiscover_tasks()
""" Load task modules from all registered Django apps """


@app.task(bind=True)
def debug_task(self):
	""" Bind tasks from all registered Django apps """
	print('Request: {0!r}'.format(self.request))