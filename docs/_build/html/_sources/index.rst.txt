.. xBI Analytics documentation master file, created by
   sphinx-quickstart on Fri Mar 19 12:13:02 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to xBI Analytics' documentation!
=========================================

Overview
============

The xBI Analytics Tools is Built on the Python (Django) Web Framework. For more information about Django, including default configurations, please see `the documentation <https://www.djangoproject.com/>`_.

======================
Steps on reproduction
======================
The basic steps on initiating a Django project are as follows:

1. Install Django ::

    $ pip install Django

2. Start Project ::

    $ django-admin startproject <project_name>

3. Start the server ::

    $ cd <project_name>

    $ python manage.py runserver



.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
