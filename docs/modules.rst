xbi_analytics
=============

.. toctree::
   :maxdepth: 4

   manage
   xbi_analytics
   users
   reports
