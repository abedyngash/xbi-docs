reports package
===============

Submodules
----------

reports.apps module
-------------------

.. automodule:: reports.apps
   :members:
   :undoc-members:
   :show-inheritance:

reports.decorators module
-------------------------

.. automodule:: reports.decorators
   :members:
   :undoc-members:
   :show-inheritance:

reports.forms module
--------------------

.. automodule:: reports.forms
   :members:
   :undoc-members:
   :show-inheritance:

reports.models module
---------------------

.. automodule:: reports.models
   :members:
   :undoc-members:
   :show-inheritance:

reports.tasks module
--------------------

.. automodule:: reports.tasks
   :members:
   :undoc-members:
   :show-inheritance:

reports.tests module
--------------------

.. automodule:: reports.tests
   :members:
   :undoc-members:
   :show-inheritance:

reports.urls module
-------------------

.. automodule:: reports.urls
   :members:
   :undoc-members:
   :show-inheritance:

reports.utils module
--------------------

.. automodule:: reports.utils
   :members:
   :undoc-members:
   :show-inheritance:

reports.views module
--------------------

.. automodule:: reports.views
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: reports
   :members:
   :undoc-members:
   :show-inheritance:
