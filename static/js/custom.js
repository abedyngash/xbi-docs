$(function() {

	$('input[type="text"]').keyup(function() {
	    this.value = this.value.toUpperCase();
	});
	$('input[type="email"]').keyup(function() {
	    this.value = this.value.toLowerCase();
	});
});

var input = document.querySelector(".phone_input");
var iti = window.intlTelInput(input, {
  // separateDialCode:true,
  utilsScript: "https://cdn.jsdelivr.net/npm/intl-tel-input@17.0.3/build/js/utils.js",
  initialCountry: 'KE'
});

// store the instance variable so we can access it in the console e.g. window.iti.getNumber()
window.iti = iti;